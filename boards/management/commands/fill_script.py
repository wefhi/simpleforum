
# this is a small script i've made to autopopulate application
# simply type python manage.py fill_script (number)
# the bigger number you type the more populous subjects and posts will be! 
# it also randomizes authors so add some more users via django admin

from django.contrib.auth.models import User
from boards.models import Board, Topic, Post
from django.core.management.base import BaseCommand, CommandError
import random
import string


# words generator 
vowels = list('aeiou')

def gen_word(min, max):
    word = ''
    syllables = min + int(random.random() * (max - min))
    for i in range(0, syllables):
        word += gen_syllable()
    
    return word.capitalize()


def gen_syllable():
    ran = random.random()
    if ran < 0.333:
        return word_part('v') + word_part('c')
    if ran < 0.666:
        return word_part('c') + word_part('v')
    return word_part('c') + word_part('v') + word_part('c')


def word_part(type):
    if type is 'c':
        return random.sample([ch for ch in list(string.ascii_lowercase) if ch not in vowels], 1)[0]
    if type is 'v':
        return random.sample(vowels, 1)[0]




class Command(BaseCommand):
    
    #give argument (how many content to generate) to our script passed via commander
    def add_arguments(self, parser):
        
        parser.add_argument('passed_number', nargs='+', type=int)
    
    def handle(self, *args, **options):
        
        passed_arg = options['passed_number']

        size = passed_arg[0]
        
        created = 0
        for a in range(random.randint(25, 35)):
            created += 1
            user = User.objects.order_by("?").first()
            board = Board.objects.create(name = gen_word(2,6) + ' to Temat numer #{}'.format(a), description = gen_word(2,6) + gen_word(2,6) + gen_word(2,6) + gen_word(2,6) + gen_word(2,6))
            for b in range(random.randint(1, size)):
                user = User.objects.order_by("?").first()
                subject = gen_word(2,6) + 'to temat #{}'.format(b)
                topic = Topic.objects.create(subject=subject, board=board, starter=user)
                for c in range(random.randint(1, size)):
                    user = User.objects.order_by("?").first()
                    Post.objects.create(message='czesc witam i czolem', topic=topic, created_by=user)
                    print('\n loaded: ' + str(created) + 'new boards')
                    